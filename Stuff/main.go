package main

import "fmt"

func zero(xPtr *int) {
	*xPtr = 0
}
func main() {
	x := 5
	zero(&x)
	fmt.Println(x) // x is 0

	var p *int
	var number int

	number = 7
	p = &number
	fmt.Println(*p)

}
